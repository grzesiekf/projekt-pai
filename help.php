<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>

<body>

   
<?php session_start(); include 'views/header.php';?>

    <div class="container flex">

        <div class="page">

            <div class="help"><a id="gotohelp1" class="goto">Częste pytania</a></div>
            <div class="help"><a id="gotohelp2" class="goto">Problemy techniczne</a></div>
            <div class="help"><a id="gotohelp3" class="goto">Pomoc klienta</a></div>
        </div>

        <div class="helpinfo">


            <div id="help1" class="help-item">

                <div class="text">
                    <h2>
                        Często zadawane pytania
                    </h2>
                    <p>

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>
                </div>
            </div>
            <div id="help2" class="help-item hidden">

                <div class="text">
                    <h2>
                        Pomoc techniczna
                    </h2>
                    <p>

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>
                </div>
            </div>

            <div id="help3" class="help-item hidden">

                <div class="text">
                    <h2>
                        Pomoc klienta
                    </h2>
                    <p>

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>
                </div>
            </div>

        </div>

        <script>
        $('#gotohelp1').click(() => {
            $('.help-item').addClass('hidden');
            $('#help1').removeClass('hidden');
        });
        $('#gotohelp2').click(() => {
            $('.help-item').addClass('hidden');
            $('#help2').removeClass('hidden');
        });
        $('#gotohelp3').click(() => {
            $('.help-item').addClass('hidden');
            $('#help3').removeClass('hidden');
        });
        </script>

    </div>


    </div>

    <?php include 'views/footer.html' ?>


</body>

</html>