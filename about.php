<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>


<?php session_start(); include 'views/header.php';?>

<div class="container">
<div class="section1">
        <img class="w40" src="public/img/book1.jpg" alt="">
        <div class="w60">
        <h6>
        Jesteśmy serwisem zajmującym się obrotem książek między ludźmi na całym świecie, zapewniając dostęp do kilku milionów tytułów w wydaniach zarówno nowych dzięki współpracy z licznymi księgarniami, jak i używanych dodawanych przez użytkowników.
Jeżeli poszukujesz rzadkiej książki, chcesz sprzedać książki już posiadane bądź po prostu kupować taniej zapraszamy do korzystania z naszego serwisu.
</h6>
            </div>
        </div>
</div>

<?php include 'views/footer.html' ?>


</body>
</html>