<?php session_start() ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>

<body>

    <?php include 'views/header.php' ?>

    <div class="container">
        <div class="section1">
            <img class="w40" src="public/img/book2.jpg" alt="">
            <div class="w60">
                <form action="search.php" method="get">
                    <div class="search search-center">
                        <input name="search" type="text" placeholder="Co możemy dla ciebie znaleźć?">
                        <img class="input-img" onclick="$('form').submit()" src="public/img/lupa.png">
                    </div>
                </form>
            </div>
        </div>
        <div class="section2">
        </div>

    </div>

    <?php include 'views/footer.html' ?>


</body>

</html>