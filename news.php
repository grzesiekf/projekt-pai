<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>

<?php session_start(); include 'views/header.php';?>

<div class="container">

<div class="news">

    <div id="news1" class="news-item">
        <div class="pic">
        <img class="ni" src="public/img/brak.png" alt="">
        </div>
        <div class="text">
        <h2>
             Nowość 1
         </h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
        </div>
    </div>
    <div id="news2" class="news-item hidden">
       <div class="pic">
       <img class="ni" src="public/img/brak.png" alt="">
         </div>
        <div class="text">
        <h2>
             Nowość 2
         </h2>
            <p>
            Ut enim ad minim veniam.
            </p>
         </div>

    </div>
    <div id="news3" class="hidden news-item" >
    <div class="pic">
    <img class="ni" src="public/img/brak.png" alt="">
     </div>
     <div class="text">
         <h2>
             Nowość 3
         </h2>
            <p>
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
            </p>
     </div>

    </div>
    <div class="page">
        <a id="goToNews1" class="goto">1</a>
        <a id="goToNews2" class="goto">2</a>
    </div>
</div>

<script>
    $('#goToNews1').click(() => {$('.news-item').addClass('hidden');$('#news1').removeClass('hidden');});
    $('#goToNews2').click(() => {$('.news-item').addClass('hidden');$('#news2').removeClass('hidden');});
</script>
    
</div>

<?php include 'views/footer.html' ?>


</body>
</html>