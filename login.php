<?php
session_start();
require_once "lib/database.php";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['login']))
    {
        $login = addslashes($_POST['login']);
        $pass = hash('sha256', $_POST['haslo']);

        // sprawdzamy czy login i hasło są dobre
        $db = new DB();
        $db->connect();

        if ($db->login($login, $pass) === true)
        {   
            $_SESSION['zalogowany'] = true;
            $_SESSION['login'] = $login;
            header("Location: index.php");
        }
        else $error = "Wrong login or password";
    }
    
}
?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>


<body style="background-image: url('public/img/bookback.jpg');background-size: cover ">
    <div class="header" style="margin-left:15%;">
        <h1>
            <a href="index.php">Books Trade</a>
        </h1>
    </div>
    <div class="login">
        <h3> Logowanie </h3>

        <form action="" method="POST">

            <div class="input">
                <?php if(isset($error))
                  echo "<div>$error</div>";
                ?>
                <input id="login" class="input__field" name="login" type="text" placeholder="Nazwa użytkownika">
                <input id="haslo" class="input__field" name="haslo" type="password" placeholder="Hasło">

            </div>

            <div class="page">
                <div class="help">
                    <div onclick='$("form").submit()' class="log">Zaloguj</div>
                </div>
                <div class="help"><a href="register.php" class="log">Nie masz jeszcze konta? Zarejestruj się</a></div>
            </div>
        </form>


    </div>


</body>

</html>