<?php
session_start();
require_once "lib/database.php";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    if (isset($_POST['login']))
    {
        $login = addslashes($_POST['login']);
        $pass = $_POST['password'];
        $pass2 = $_POST['password2'];
        $name = $_POST['imie'];
        $surname = $_POST['nazwisko'];
        $tel = $_POST['tel'];
        $email = $_POST['email'];
        $nrkontabankowego = $_POST['konto'];

        if($pass !== $pass2) {
            $error = "Hasła nie pokrywają się";
        }
        else {
            $pass = hash('sha256', $_POST['password']);
    
            $db = new DB();
            $db->connect();
    
            $query = "INSERT INTO user (login, pass, email) VALUES ('$login', '$pass', '$email')";
            $result = $db->query($query);

            if($result === true) {
                            $query = "SELECT iduser from user WHERE login = '$login'";
                            $result = $db->query($query);
                            $id = $result -> fetch_assoc()['iduser'];
                            
                            $query = "INSERT INTO dane (iduser, imie, nazwisko, tel, nrkonta) VALUES ('$id', '$name', '$surname', '$tel', '$nrkontabankowego')";
                            $result = $db->query($query);
                    
                            if ($result === true)
                            {       
                                header("Location: index.php");
                            }
                           
            }
            else {
                $error = "Login jest zajęty";
            }
          
        }

    }
    
}
?>
<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="public/css/style.css" />
    <title>Booktrade</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>

<body>

    <div class="register">

        <h2> Rejestracja </h2>

        <form action="" method="POST">
        <?php if(isset($error)) echo "<div>$error</div>";?>

            <input id="user" class="input__field" name="login" type="text" placeholder="Nazwa użytkownika *">
            <input id="email" class="input__field" name="email" type="email" placeholder="Email *">
            <input id="pas" class="input__field" name="password" type="password" placeholder="Hasło *">
            <input id="pas2" class="input__field" name="password2" type="password" placeholder="Powtórz hasło *">
            <input id="imie" class="input__field" name="imie" type="text" placeholder="Imie">
            <input id="nazwisko" class="input__field" name="nazwisko" type="text" placeholder="Nazwisko">
            <input id="tel" class="input__field" name="tel" type="tel" placeholder="Nr telefonu">
            <input id="konto" class="input__field" name="konto" type="text" placeholder="Nr konta bankowego">


            <div class="help">
                <div onclick='$("form").submit()' class="log">Załóż darmowe konto</div>
            </div>
        </form>
        <p> * - pole wymagane</p>
    </div>
</body>

</html>