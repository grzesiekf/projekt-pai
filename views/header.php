<div class="header">

    <h1>  
      <a href="index.php">Books Trade</a>
    </h1>
    
    <?php
  if(isset($_SESSION['login'])):
?>
  <a href="logout.php" class="register">Wyloguj</a>

  <div class="breadcrumb">
    <a href="news.php">Aktualności</a>
    <a href="help.php">Pomoc</a>
    <a href="rule.php">Regulamin</a>
    <a href="panel.php">Profil</a>
  </div>
</div>
<?php else: ?>
  <a href="register.php" class="register">Rejestracja</a>

  <div class="breadcrumb">
    <a href="news.php">Aktualności</a>
    <a href="help.php">Pomoc</a>
    <a href="rule.php">Regulamin</a>
    <a href="login.php">Logowanie</a>
  </div>
</div>
<?php endif; ?>